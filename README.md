This is a mod for Synchrony which changes the axes attack pattern from:

```
  X
PXX
  X
```
to
```
  X
PX
  X
```
This nerf would hopefully result in axes being a more normal weapon rather than an overpowered outlier like they are in the base game.
The swipe texture is modified to not swipe in the middle tile, which will reduce confusion and remind the player that the mod is active.
