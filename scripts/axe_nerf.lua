local event = require "necro.event.Event"
local action = require "necro.game.system.Action"

event.entitySchemaLoadItem.override("weaponPatternAxe", {sequence = 1}, function(func, ev)
    func(ev)
    if ev.entity.name:sub(-3) == "Axe" and ev.entity.name:sub(0,6) == "Weapon" then
		ev.entity.weaponPattern.pattern.tiles = {
                    {swipe = "dagger", offset = {1,0}, multiHit = false}, 
                    {offset = {2,1}, clearance = {{1,0}}, dashDirection = action.Direction.RIGHT}, 
                    {offset = {2,-1},  clearance = {{1,0}}, dashDirection = action.Direction.RIGHT}
                } 
		ev.entity.weaponPattern.pattern.swipe = "axe"
    end
end)

event.swipe.add("axe", "axe", function(ev)
        ev.entity.swipe.texture = "mods/AxeNerf/gfx/swipe_axe_titanium.png"
        ev.entity.swipe.frameCount = 3
        ev.entity.swipe.width = 48
        ev.entity.swipe.height = 72
        ev.entity.swipe.duration = 200
end)
 